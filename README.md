# SUCKLESS-BUILDS #

### Summary ###

Personal builds of suckless software.

Most of the patches come from the Suckless site, I have only made slight modifications to make them work together. The patch for surf that adds the option to scroll to the top or the bottom of the page is mine, and many of the configuration files are customized in regards to keybindings and such.

The versions of the programs I use are the generally the latest ones available in the official Gentoo repositories.

### Installation ###

Apply the patches in the order they are numbered in, then apply the configuration. On Gentoo, for example, you would install my build of dwm by copying the patches under `/etc/portage/patches/x11-wm/dwm/`, then running `emerge dwm`, then copying the configuration file `dwn-6.1` under `/etc/portage/savedconfig/x11-wm/`, and finally running `emerge dwm` again.
